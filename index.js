const express = require('express')
const mongoose = require('mongoose')
const compression = require('compression')
const cors = require('cors')

const app = express()

// connect to the data base
const connectToDatabase = async () => {
    try {
        await mongoose.connect('mongodb+srv://creatosaurus1:EOaVFfQ5YhOD3UhF@creatosaurus.7trc5.mongodb.net/apollo?authSource=admin',
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true
            })
        console.log("connected to data base")
    } catch (error) {
        console.log(error)
    }
}

connectToDatabase()
app.use(cors())
app.use('/apollo/static', express.static('demo'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json({ limit: '500mb' }));
app.use(compression())
app.use('/apollo', require('./Router/audio'))
app.use('/apollo/video', require('./Router/createVideo'))
app.use('/apollo/muse', require('./Router/createMuseImage'))

app.listen(4004 || process.env.PORT, () => {
    console.log("Listening on port 4004")
})