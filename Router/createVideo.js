const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const fs = require('fs')
const path = require('path')
const express = require('express')
const router = express.Router()
const puppeteer = require('puppeteer'); 
const Video = require('../Schema/video')
const mongoose = require('mongoose')
const { downloadFileFromS3 } = require('../Functions/S3')
const { exec } = require('child_process')
const constant = require('../Constant')

router.post('/create', async (req, res) => {
  let videoName
  try {
    //global variables
    let duration = 0
    let arrayOfSeen = req.body.template //the template data to render
    videoName = mongoose.Types.ObjectId()

    //video template object generation
    let videoObject = {
      _id: videoName,
      userId: req.body.userId,
      //url: `https://localhost:4004/apollo/video/${videoName}.mp4`,
      url: `${constant.url}video/${videoName}.mp4`,
      status: false,
      template: JSON.stringify(arrayOfSeen),
      canvasWidth: req.body.canvasWidth,
      canvasHeight: req.body.canvasHeight,
      canvasMultiply: req.body.canvasMultiply,
      projectName: req.body.projectName,
      videoPath: `../Videos/${videoName}.mp4`,
      audio: JSON.stringify(req.body.audio)
    }

    //save the video template and information to the db
    await Video.create(videoObject)
    res.send("done")

    //get the time of the duration of the video to be generated as per seen wise
    let audioNameTest = req.body.audio.src
    audioNameTest = audioNameTest.split('/').pop()
    await downloadFileFromS3(audioNameTest)
    req.body.template.map(data => {
      duration = parseInt(data.duration) + duration
    })

    let seconds = ((duration % 60000) / 1000).toFixed(0);
    //assign the canvas height width and scaling
    const height = parseInt(req.body.canvasHeight)
    const width = parseInt(req.body.canvasWidth)
    const multiplyer = parseInt(req.body.canvasMultiply)


    // create the directory and the txt file temporary
    let i = 0 //this is set to give the name to the images
    fs.mkdirSync(`./${videoName}`);
    //fs.writeFileSync(`${videoName}.txt`)
    fs.openSync(`${videoName}.txt`, 'w')

    const getTheImageAndStoreToTheFile = (base) => {
      var base64Data = base.replace(/^data:image\/jpeg;base64,/, "");
      fs.appendFileSync(`${videoName}.txt`, `file ./${videoName}/image${i}.jpeg \n`)
      fs.writeFile(`./${videoName}/image${i}.jpeg`, base64Data, 'base64', function (err) {
      });
      i++
    }

    // set up the browser
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox'],
      //executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
      executablePath: '/usr/bin/chromium-browser'
    });

    //open new page in the browser
    const page = await browser.newPage();
    page.setViewport({ width: width * multiplyer, height: height * multiplyer });
    await page.exposeFunction("getTheImageAndStoreToTheFile", getTheImageAndStoreToTheFile); // call the external function from page evaluation

    // the script for the creating canvas and rendering template
    await page.evaluate(async (arrayOfSeen, width, height, multiplyer) => {
      // Global variables
      let endTime = 0

      // create the canvas as per the template
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      canvas.width = width * multiplyer;
      canvas.height = height * multiplyer;
      document.body.appendChild(canvas)

      let allReadyCreatedElementWithThisId = []

      const createElementToDom = async (data) => {
        data.map(data => {
          if (allReadyCreatedElementWithThisId.includes(data.elementId)) return
          allReadyCreatedElementWithThisId.push(data.elementId)
          if (data.type === "image") {
            var x = document.createElement("img");
            x.setAttribute("id", data.elementId);
            x.src = data.src;
            x.crossOrigin = 'anonymous'
            x.setAttribute("width", "320");
            x.setAttribute("height", "240");
            document.body.appendChild(x);
          } else if (data.type === "video") {
            var x = document.createElement("VIDEO");
            x.setAttribute("id", data.elementId);
            if (x.canPlayType("video/mp4")) {
              x.src = data.src
            }
            x.setAttribute("width", "320");
            x.setAttribute("height", "240");
            x.setAttribute("controls", "controls");
            x.autoplay = false;
            x.preload = "none"
            x.crossOrigin = 'anonymous'
            document.body.appendChild(x);
          } else if (data.type === "svg") {
            var x = document.createElement("img");
            x.setAttribute("id", data.elementId);
            x.src = data.svgImageUrl;
            x.crossOrigin = 'anonymous'
            x.setAttribute("width", "320");
            x.setAttribute("height", "240");
            document.body.appendChild(x);
          } else if (data.type === "mask") {
            var x = document.createElement("img");
            x.setAttribute("id", data.elementId);
            x.src = data.src;
            x.crossOrigin = 'anonymous'
            x.setAttribute("width", "320");
            x.setAttribute("height", "240");
            document.body.appendChild(x);

            var x1 = document.createElement("img");
            x1.setAttribute("id", data.elementId2);
            x1.src = data.src1;
            x1.crossOrigin = 'anonymous'
            x1.setAttribute("width", "320");
            x1.setAttribute("height", "240");
            document.body.appendChild(x1);
          } else if (data.type === "group") {
            data.data.map(element => {
              if (element.type === "image") {
                var x = document.createElement("img");
                x.setAttribute("id", element.elementId);
                x.src = element.src;
                x.crossOrigin = 'anonymous'
                x.setAttribute("width", "320");
                x.setAttribute("height", "240");
                document.body.appendChild(x);
              } else if (element.type === "video") {
                var x = document.createElement("VIDEO");
                x.setAttribute("id", element.elementId);
                if (x.canPlayType("video/mp4")) {
                  x.src = element.src
                }
                x.setAttribute("width", "320");
                x.setAttribute("height", "240");
                x.setAttribute("controls", "controls");
                x.autoplay = false;
                x.preload = "none"
                x.crossOrigin = 'anonymous'
                document.body.appendChild(x);
              } else if (element.type === "svg") {
                var x = document.createElement("img");
                x.setAttribute("id", element.elementId);
                x.src = element.svgImageUrl;
                x.crossOrigin = 'anonymous'
                x.setAttribute("width", "320");
                x.setAttribute("height", "240");
                document.body.appendChild(x);
              } else if (element.type === "mask") {
                var x = document.createElement("img");
                x.setAttribute("id", element.elementId);
                x.src = element.src;
                x.crossOrigin = 'anonymous'
                x.setAttribute("width", "320");
                x.setAttribute("height", "240");
                document.body.appendChild(x);

                var x1 = document.createElement("img");
                x1.setAttribute("id", element.elementId2);
                x1.src = element.src1;
                x1.crossOrigin = 'anonymous'
                x1.setAttribute("width", "320");
                x1.setAttribute("height", "240");
                document.body.appendChild(x1);
              }
            })
          }
        })
      }

      arrayOfSeen.map(data => {
        endTime = endTime + parseInt(data.duration)
        createElementToDom(data.arrayOfElements)
      })

      //fetch all the videos before start creating template
      for (const data of arrayOfSeen) {
        for (const data1 of data.arrayOfElements) {
          if (data1.type === "video") {
            let videoToBeLoaded = document.getElementById(data1.elementId)
            let blob = await fetch(data1.src).then(r => r.blob());
            var videoUrl = URL.createObjectURL(blob);
            videoToBeLoaded.src = videoUrl
          }
        }
      }

      let calculateEndTime = endTime / 33.33
      endTime = calculateEndTime * 2000 // define the time to wait the browser and we programitcally close it
      await new Promise(resolve => setTimeout(resolve, 10000)); // wait for the element to load in the canvas

      arrayOfSeen.forEach(data1 => {
        data1.arrayOfElements.forEach(element => {
          if (element.type === "video") {
            let video = document.getElementById(element.elementId)
            video.pause()
            video.currentTime = 0
            element.showVideo = true
          }
          if (element.animation) {
            if (element.animation.type === "fromLeft") {
              element.animation = { play: true, x: 0 - (element.width * multiplyer), y: 0, type: "fromLeft", speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "fromRight") {
              element.animation = { play: true, x: canvas.width, y: 0, type: "fromRight", speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "fromTop") {
              element.animation = { play: true, x: 0, y: 0 - (element.height * multiplyer), type: "fromTop", speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "fromBottom") {
              element.animation = { play: true, x: 0, y: canvas.height, type: "fromBottom", speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "fadeIn") {
              element.animation = { play: true, type: "fadeIn", opacity: 0, speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "fadeOut") {
              element.animation = { play: true, type: "fadeOut", opacity: 1, speed: element.animation.speed, delay: 2000, time: element.animation.time }
            } else if (element.animation.type === "rotate") {
              element.animation = { play: true, type: "rotate", rotate: 0, speed: element.animation.speed, time: element.animation.time, delay: 2000 }
            }
          }
        })
      })

      await new Promise(resolve => setTimeout(resolve, 5000)); // wait for the element to load in the canvas

      // Animation started 
      //@Bottom To Top
      const moveTheAnimationBottomToTopImage = (image, img) => {
        image.animation.y = image.animation.y - (image.animation.speed * multiplyer)
        if (image.y * multiplyer < image.animation.y) {
          ctx.drawImage(img, image.x * multiplyer, image.animation.y, image.width * multiplyer, image.height * multiplyer);
        } else {
          ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        }
      }

      const moveTheAnimationBottomToTopText = (txt, x, y, data) => {
        if (y < data.animation.y) {
          ctx.fillText(txt, x, data.animation.y)
        } else {
          ctx.fillText(txt, x, y);
        }
      }

      //@Top To Bottom
      const moveTheAnimationTopToBottomImage = (image, img) => {
        image.animation.y = image.animation.y + (image.animation.speed * multiplyer)
        if (image.y * multiplyer > image.animation.y) {
          ctx.drawImage(img, image.x * multiplyer, image.animation.y, image.width * multiplyer, image.height * multiplyer);
        } else {
          ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        }
      }

      const moveTheAnimationTopToBottomText = (txt, x, y, data) => {
        if (y > data.animation.y) {
          ctx.fillText(txt, x, data.animation.y)
        } else {
          ctx.fillText(txt, x, y);
        }
      }

      //@Left To Right
      const moveTheAnimationLeftToRightImage = (image, img) => {
        image.animation.x = image.animation.x + (image.animation.speed * multiplyer)
        if (image.x * multiplyer > image.animation.x) {
          ctx.drawImage(img, image.animation.x, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        } else {
          ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        }
      }

      const moveTheAnimationLeftToRightText = (txt, x, y, data) => {
        if (x > data.animation.x) {
          ctx.fillText(txt, data.animation.x, y);
        } else {
          ctx.fillText(txt, x, y);
        }
      }

      //@Right To Left
      const moveTheAnimationRightToLeftImage = (image, img) => {
        image.animation.x = image.animation.x - (image.animation.speed * multiplyer)
        if (image.x * multiplyer < image.animation.x) {
          ctx.drawImage(img, image.animation.x, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        } else {
          ctx.drawImage(img, image.x * multiplyer, image.y * multiplyer, image.width * multiplyer, image.height * multiplyer);
        }
      }

      const moveTheAnimationRightToLeftText = (txt, x, y, data) => {
        if (x < data.animation.x) {
          ctx.fillText(txt, data.animation.x, y);
        } else {
          ctx.fillText(txt, x, y);
        }
      }

      //@Fade Out
      const fadeOutImage = (data, img) => {
        data.animation.opacity = data.animation.opacity - data.animation.speed
        if (data.animation.opacity > 0) {
          ctx.globalAlpha = data.animation.opacity
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
        } else {
          ctx.globalAlpha = 0
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
        }
      }

      const fadeOutText = (txt, x, y, data) => {
        if (data.animation.opacity > 0) {
          ctx.globalAlpha = data.animation.opacity
          ctx.fillText(txt, x, y);
          ctx.globalAlpha = 1
        } else {
          ctx.globalAlpha = 0
          ctx.fillText(txt, x, y);
          ctx.globalAlpha = 1
        }
      }

      //@Fade In
      const fadeInImage = (data, img) => {
        data.animation.opacity = data.animation.speed + data.animation.opacity
        if (data.animation.opacity < 1) {
          ctx.globalAlpha = data.animation.opacity
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
          ctx.globalAlpha = 1
        } else {
          ctx.globalAlpha = 1
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
        }
      }

      const fadeInText = (txt, x, y, data) => {
        if (data.animation.opacity < 1) {
          ctx.globalAlpha = data.animation.opacity
          ctx.fillText(txt, x, y);
          ctx.globalAlpha = 1
        } else {
          ctx.globalAlpha = 1
          ctx.fillText(txt, x, y);
        }
      }

      //@Rotate Animation
      const rotateImage = (data, img) => {
        data.animation.rotate = data.animation.rotate + data.animation.speed
        if (data.animation.rotate < 360) {
          ctx.translate((data.x * multiplyer) + (data.width * multiplyer / 2), (data.y * multiplyer) + (data.height * multiplyer / 2))
          ctx.rotate((Math.PI / 180) * data.animation.rotate);
          ctx.translate(-(data.x * multiplyer) - (data.width * multiplyer / 2), -(data.y * multiplyer) - (data.height * multiplyer / 2));
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
        } else {
          ctx.drawImage(img, data.x * multiplyer, data.y * multiplyer, data.width * multiplyer, data.height * multiplyer);
        }
      }

      const rotateText = (txt, x, y, data) => {
        if (data.animation.rotate < 360) {
          ctx.translate(data.x + (data.width / 2), data.y + (data.height / 2))
          ctx.rotate((Math.PI / 180) * data.animation.rotate);
          ctx.translate(-data.x - (data.width / 2), -data.y - (data.height / 2));
          ctx.fillText(txt, x, y);
        } else {
          ctx.fillText(txt, x, y);
        }
      }

      //Animation ended

      //elements effects started
      const rotateElement = (data) => {
        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer

        ctx.translate(x + (width / 2), y + (height / 2))
        ctx.rotate((Math.PI / 180) * data.rotate);
        ctx.translate(-x - (width / 2), -y - (height / 2));
      }

      const blurElement = (data) => {
        ctx.filter = `blur(${data.blur}px)`
      }

      const verticalFlipElement = (img, data) => {
        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer

        ctx.translate(x + width / 2, y + height / 2);
        ctx.scale(1, -1);

        // Move registration point back to the top left corner of canvas
        ctx.translate((-width) / 2, (-height) / 2);
        ctx.globalAlpha = data.opacity
        ctx.drawImage(img, 0, 0, width, height);
        ctx.globalAlpha = 1.0;
      }

      const horizontalFlipElement = (img, data) => {
        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer

        ctx.translate(x + width / 2, y + height / 2);
        ctx.scale(-1, 1);

        // Move registration point back to the top left corner of canvas
        ctx.translate((-width) / 2, (-height) / 2);
        ctx.globalAlpha = data.opacity
        ctx.drawImage(img, 0, 0, width, height);
        ctx.globalAlpha = 1.0;
      }


      const shadowToElement = (data) => {
        ctx.shadowColor = data.shadowColor;
        ctx.shadowBlur = data.shadowBlur;
        ctx.shadowOffsetX = data.shadowOffsetX * multiplyer;
        ctx.shadowOffsetY = data.shadowOffsetY * multiplyer;
      }

      //check the element to show on the canvas or not
      const checkTheElementHaveToDrawOnCanvasOrNot = (data) => {
        if (data.endTime !== null) {
          if (data.endTime / 30 < canvasSeenFrames) {
            data.showElementPreview = false
            return false
          }
        }

        if (data.showElementPreview === false) return false

        if (data.startTime === null) {
          return true
        } else {
          if (data.startTime / 30 < canvasSeenFrames) {
            data.showElementPreview = true
            return true
          }
        }
      }

      //checkTheAnimationTypeofImage
      const animationTypeOfImage = (data, img) => {
        if (data.animation.type === "fromLeft") {
          moveTheAnimationLeftToRightImage(data, img)
        } else if (data.animation.type === "fromRight") {
          moveTheAnimationRightToLeftImage(data, img)
        } else if (data.animation.type === "fromTop") {
          moveTheAnimationTopToBottomImage(data, img)
        } else if (data.animation.type === "fromBottom") {
          moveTheAnimationBottomToTopImage(data, img)
        } else if (data.animation.type === "fadeIn") {
          fadeInImage(data, img)
        } else if (data.animation.type === "fadeOut") {
          fadeOutImage(data, img)
        } else if (data.animation.type === "rotate") {
          rotateImage(data, img)
        }
      }

      const drawImageOnCanvas = (data) => {
        //check the element is capable of displaying on canvas or not as per time
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer
        let img = document.getElementById(data.elementId);

        rotateElement(data)
        shadowToElement(data)
        ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
        if (data.verticalFlip === true) {
          verticalFlipElement(img, data)
        } else if (data.horizontalFlip === true) {
          horizontalFlipElement(img, data)
        } else {
          if (data.animation !== undefined) {
            animationTypeOfImage(data, img)
          } else {
            ctx.globalAlpha = data.opacity
            ctx.drawImage(img, x, y, width, height);
            ctx.globalAlpha = 1.0;
          }
        }
      }

      const drawVideoOnCanvas = (data) => {
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return
        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer
        let video = document.getElementById(data.elementId);

        /*if (data.showVideo) {
          if (data.startVideo !== null) {
            video.currentTime = data.startVideo
          }
          data.showVideo = false
        }
        if (data.endVideo !== null) {
          if (video.currentTime > data.endVideo) return
        }*/

        ctx.save()
        rotateElement(data)
        shadowToElement(data)
        ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
        if (data.verticalFlip === true) {
          verticalFlipElement(video, data)
        } else if (data.horizontalFlip === true) {
          horizontalFlipElement(video, data)
        } else {
          if (data.animation !== undefined) {
            animationTypeOfImage(data, img)
          } else {
            ctx.globalAlpha = data.opacity
            ctx.drawImage(video, x, y, width, height);
            ctx.globalAlpha = 1.0;
          }
        }
        ctx.restore()
        video.currentTime += 1 / 30
      }

      //check the animation type of text
      const checkTheAnimationTypeOfText = (txt, x, y, data) => {
        if (data.animation.type === "fromLeft") {
          moveTheAnimationLeftToRightText(txt, x, y, data)
        } else if (data.animation.type === "fromRight") {
          moveTheAnimationRightToLeftText(txt, x, y, data)
        } else if (data.animation.type === "fromTop") {
          moveTheAnimationTopToBottomText(txt, x, y, data)
        } else if (data.animation.type === "fromBottom") {
          moveTheAnimationBottomToTopText(txt, x, y, data)
        } else if (data.animation.type === "fadeIn") {
          fadeInText(txt, x, y, data)
        } else if (data.animation.type === "fadeOut") {
          fadeOutText(txt, x, y, data)
        } else if (data.animation.type === "rotate") {
          rotateText(txt, x, y, data)
        }
      }

      const checkTheAnimationTypeOfTextAndIncreaseData = (data) => {
        if (data.animation.type === "fromLeft") {
          data.animation.x = data.animation.x + (data.animation.speed * multiplyer)
        } else if (data.animation.type === "fromRight") {
          data.animation.x = data.animation.x - (data.animation.speed * multiplyer)
        } else if (data.animation.type === "fromTop") {
          data.animation.y = data.animation.y + (data.animation.speed * multiplyer)
        } else if (data.animation.type === "fromBottom") {
          data.animation.y = data.animation.y - (data.animation.speed * multiplyer)
        } else if (data.animation.type === "fadeIn") {
          data.animation.opacity = data.animation.speed + data.animation.opacity
        } else if (data.animation.type === "fadeOut") {
          data.animation.opacity = data.animation.opacity - data.animation.speed
        } else if (data.animation.type === "rotate") {
          data.animation.rotate = data.animation.rotate + data.animation.speed
        }
      }

      const drawTextOnCanvas = (data) => {
        //check the element is capable of displaying on canvas or not as per time
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return
        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let size = data.size * multiplyer
        let width = 0

        let txt = data.content
        ctx.save()
        canvas.style.letterSpacing = (data.letterSpacing * multiplyer) + 'px';
        ctx.globalAlpha = data.opacity
        ctx.textBaseline = data.baseline;
        ctx.font = data.italic + " " + data.bold + " " + size + "px " + data.fontFamily
        ctx.fillStyle = data.color;
        ctx.textAlign = data.align

        blurElement(data)
        shadowToElement(data)

        //convert the text in array if multiline is there
        let data1 = txt.split("\n")
        let len = data1.length * size
        data.height = len

        if (data1.length <= 1) {
          ctx.textAlign = "left"
          ctx.textBaseline = "top";
          width = ctx.measureText(txt).width
          data.width = Math.ceil(width)

          if (data.animation !== undefined) {
            checkTheAnimationTypeOfTextAndIncreaseData(data)
          }

          if (data.animation !== undefined) {
            checkTheAnimationTypeOfText(txt, x, y, data)
          } else {
            //to rotate the text
            rotateElement(data)
            ctx.fillText(txt, x, y);
          }
        } else {
          //get the largest width from and array
          if (data.list) {
            for (let i = 0; i < data1.length; i++) {
              if (width < ctx.measureText(i + 1 + ". " + data1[i]).width) {
                width = ctx.measureText(i + 1 + ". " + data1[i]).width
                data.width = Math.ceil(width)
              }
            }
          } else {
            for (let i = 0; i < data1.length; i++) {
              if (width < ctx.measureText(data1[i]).width) {
                width = ctx.measureText(data1[i]).width
                data.width = Math.ceil(width)
              }
            }
          }

          if (data.animation !== undefined) {
            checkTheAnimationTypeOfTextAndIncreaseData(data)
          }

          rotateElement(data)
          let lineHeight = size + data.lineHeight
          for (let i = 0; i < data1.length; i++) {
            if (data.list) {
              if (data.align === "center") {
                ctx.fillText(i + 1 + ". " + data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
              } else if (data.align === "right") {
                ctx.fillText(i + 1 + ". " + data1[i], x + width, y + i * lineHeight);
              } else if (data.align === "left") {
                ctx.fillText(i + 1 + ". " + data1[i], x, y + i * lineHeight);
              }
            } else {
              if (data.align === "center") {
                if (data.animation !== undefined) {
                  checkTheAnimationTypeOfText(data1[i], x + (width / 2), y + (len / 2) + i * lineHeight, data)
                } else {
                  ctx.fillText(data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
                }
              } else if (data.align === "right") {
                if (data.animation !== undefined) {
                  checkTheAnimationTypeOfText(data1[i], x + width, y + i * lineHeight, data)
                } else {
                  ctx.fillText(data1[i], x + width, y + i * lineHeight);
                }
              } else if (data.align === "left") {
                if (data.animation !== undefined) {
                  checkTheAnimationTypeOfText(data1[i], x, y + i * lineHeight, data)
                } else {
                  ctx.fillText(data1[i], x, y + i * lineHeight);
                }
              }
            }
          }
        }
        canvas.style.letterSpacing = 0 + 'px';
        ctx.restore()
      }


      const drawSvgOnCanvas = (data) => {
        //check the element is capable of displaying on canvas or not as per time
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer

        let img = document.getElementById(data.elementId)

        ctx.save()
        rotateElement(data)
        blurElement(data)
        shadowToElement(data)
        ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`

        if (data.verticalFlip === true) {
          verticalFlipElement(img, data)
        } else if (data.horizontalFlip === true) {
          horizontalFlipElement(img, data)
        } else {
          if (data.animation !== undefined) {
            animationTypeOfImage(data, img)
          } else {
            ctx.globalAlpha = data.opacity
            ctx.drawImage(img, x, y, width, height);
            ctx.globalAlpha = 1.0;
          }
        }
        ctx.restore()
      }

      const previewDrawMaskOnCanvas = (data) => {
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

        let x = data.x * multiplyer
        let y = data.y * multiplyer
        let width = data.width * multiplyer
        let height = data.height * multiplyer
        let img = document.getElementById(data.elementId)
        let img1 = document.getElementById(data.elementId2)

        const canvas2 = new OffscreenCanvas(width, height);
        let ctx2 = canvas2.getContext("2d");

        canvas2.height = height
        canvas2.width = width
        ctx2.save()
        ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
        ctx2.fillRect(0, 0, canvas2.width, canvas2.height);
        ctx2.globalCompositeOperation = 'source-in';

        ctx2.drawImage(img, 0, 0, width, height);
        ctx2.drawImage(img1, 0, 0, width, height);
        ctx2.globalCompositeOperation = 'source-over';
        ctx2.restore()

        ctx.save()
        rotateElement(data)
        shadowToElement(data)
        if (data.animation !== undefined) {
          animationTypeOfImage(data, canvas2)
        } else {
          ctx.drawImage(canvas2, x, y, width, height);
        }
        ctx.restore()
      }

      const drawGroupOnCanvas = (data) => {
        //check the element is capable of displaying on canvas or not as per time
        if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

        data.data.forEach(element => {
          if (element.type === "image") {
            drawImageOnCanvas(element)
          } else if (element.type === "video") {
            drawVideoOnCanvas(element)
          } else if (element.type === "text") {
            drawTextOnCanvas(element)
          } else if (element.type === "svg") {
            drawSvgOnCanvas(element)
          } else if (element.type === "mask") {
            previewDrawMaskOnCanvas(element)
          }
        })
      }

      // Drawing Functiones of the canvas end
      //@desc takes the image of the canvas by 200 millisecondes and draw it according to the 30fps 
      let n = 0
      let canvasAllFrames = 0
      let canvasSeenFrames = 0
      let seenTime = parseInt(arrayOfSeen[n].duration)

      const Preview = (data1, backgroundColor) => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvasAllFrames = canvasAllFrames + 1
        canvasSeenFrames = canvasSeenFrames + 1

        data1.map(data => {
          if (data.type === "image") {
            drawImageOnCanvas(data)
          } else if (data.type === "video") {
            drawVideoOnCanvas(data)
          } else if (data.type === "text") {
            drawTextOnCanvas(data)
          } else if (data.type === "svg") {
            drawSvgOnCanvas(data)
          } else if (data.type === "mask") {
            previewDrawMaskOnCanvas(data)
          } else if (data.type === "group") {
            drawGroupOnCanvas(data)
          }
        })

        getTheImageFromCanvas() // call the ones so the image fetching process can be start
      }

      const getTheImageFromCanvas = async () => {
        let data = canvas.toDataURL('image/jpeg', 1.0)
        getTheImageAndStoreToTheFile(data);
        await new Promise(resolve => setTimeout(resolve, 300));
        callTheSeens()
      }

      let resolveTheTimeOut

      const callTheSeens = () => {
        if (seenTime / (1000 / 30) > canvasAllFrames) {
          let arrayOfElements = arrayOfSeen[n].arrayOfElements
          let backgroundColor = arrayOfSeen[n].backgroundColor
          Preview(arrayOfElements, backgroundColor)
        } else {
          n = n + 1
          if (n >= arrayOfSeen.length) return resolveTheTimeOut()
          canvasSeenFrames = 0
          seenTime = parseInt(arrayOfSeen[n].duration) + seenTime
          let arrayOfElements = arrayOfSeen[n].arrayOfElements
          let backgroundColor = arrayOfSeen[n].backgroundColor
          Preview(arrayOfElements, backgroundColor)
        }
      }


      //@desc take the first seen and draw
      callTheSeens()
      await new Promise(resolve => timeOut = setTimeout(resolveTheTimeOut = resolve, endTime)); // wait till the image processing finish
      return
    }, arrayOfSeen, width, height, multiplyer)

    await browser.close() // close the browser once rendering completed

    let audioName = req.body.audio.src
    audioName = audioName.split('/').pop()
    var proc = new ffmpeg();

    //stich all the images and create the video and delete all folders after creating the video
    proc.addInput(`${videoName}.txt`)
      .on('start', function (ffmpegCommand) {
        //console.log(ffmpegCommand)
      })
      .on('progress', function (data) {
        //console.log(data)
      })
      .on('end', function () {
        updateVideoStatus(videoName)
        fs.unlinkSync(`${videoName}.txt`)
        fs.rmdirSync(`${videoName}`, { recursive: true });
      })
      .on('error', function (error) {
        console.log(error)
        fs.unlinkSync(`${videoName}.txt`)
        fs.rmdirSync(`${videoName}`, { recursive: true });
      })

      //add the multiple sound at a single time
      .withFpsInput(30) // fps matching your jpg or png input sequence  
      .addInputOption(['-f concat', '-safe 0'])
      .outputOptions(['-c:v libx264', '-r 30', '-pix_fmt yuv420p'])
      .input(fs.createReadStream(path.join(__dirname, `../Audio/${audioName}`))).inputOptions(`-t ${seconds}`)
      .output(`./Videos/${videoName}.mp4`)
      .run();


    const updateVideoStatus = async (videoName) => {
      let data = await Video.findOne({ _id: videoName })
      data.status = true
      await data.save()
    }
  } catch (error) {
    fs.unlinkSync(`${videoName}.txt`)
    fs.rmdirSync(`${videoName}`, { recursive: true });
  }
})

router.get('/:fileName', async (req, res) => {
  try {
    res.sendFile(path.join(__dirname, `../Videos/${req.params.fileName}`))
  } catch (error) {
    res.status(500).json({
      error: error
    })
  }
})

router.get('/download/videos', async (req, res) => {
  try {
    let data = await Video.find({}, { url: 1, _id: 1, status: 1 })
    res.send(data)
  } catch (error) {
    res.status(500).json({
      error: error
    })
  }
})

router.get('/template/:width/:height', async (req, res) => {
  try {
    const limit = parseInt(req.query.limit) || 10
    const skip = limit * (parseInt(req.query.page) - 1) || 0

    const data = await Video.find({ canvasWidth: req.params.width, canvasHeight: req.params.height }).skip(skip).limit(limit)
    res.send(data)
  } catch (error) {
    res.status(500).json({
      error: error
    })
  }
})

module.exports = router