const { v4: uuidv4 } = require('uuid');
const S3 = require('aws-sdk/clients/s3')
const fs = require('fs')

// bucket details apollo
const bucketName = "creatosaurus-apollo"
const region = "ap-south-1"
const accessKeyId = "AKIAQ5RO3HORQEFLRPHH"
const secretAccessKey = "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"

// bucket details cache
const bucketNameCache = 'creatosaurus-cache';
const regionCache = 'ap-south-1';
const accessKeyIdCache = 'AKIAQ5RO3HORQNXCXHGQ';
const secretAccessKeyCache = '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';

const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey
})

const s3Cache = new S3({
  region:regionCache,
  accessKeyId:accessKeyIdCache,
  secretAccessKey:secretAccessKeyCache
})

//upload a file to s3
const generateuploadURLToS3 = async () => {
  const imageName = uuidv4()
  const expireSecondsOfURL = 60 * 30

  const params = ({
    Bucket: bucketName,
    Key: imageName,
    Expires: expireSecondsOfURL
  })

  const uploadURL = await s3.getSignedUrlPromise('putObject', params)
  return { uploadURL, imageName }
}

const deleteFile = async (fileKey) => {
  try {
    const deleteParams = {
      Key: fileKey,
      Bucket: bucketName
    }

    await s3.deleteObject(deleteParams).promise()
  } catch (error) {
    console.log(error)
  }
}

// downloads a file from s3
const downloadFileFromS3 = (fileKey) => {
  const downloadParams = {
    Key: fileKey,
    Bucket: bucketName
  }
  return new Promise((resolve, reject) => {
    s3.getObject(downloadParams, function (err, data) {
      if (err) {
        reject(Error("It broke"));
      } else {
        fs.writeFileSync(`./Audio/${fileKey}`, data.Body)
        resolve("Stuff worked!");
      }
    })
  })
}


const addBase64DataToS3Bucket = async (base64) => {
  try {
    const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = base64.split(';')[0].split('/')[1];

    const params = {
      Bucket: bucketNameCache,
      Key: uuidv4(),
      Body: base64Data,
      ContentType: `image/${type}`
    }

    const { Location } = await s3Cache.upload(params).promise();
    return Location
  } catch (error) {
    console.log(error)
    return base64
  }
}


module.exports = {
  generateuploadURLToS3,
  addBase64DataToS3Bucket,
  deleteFile,
  downloadFileFromS3
}