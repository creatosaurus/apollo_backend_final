const mongoose = require('mongoose')
const schema = mongoose.Schema

const Video = new schema({
    userId: String,
    url: String,
    status:Boolean,
    template: String,
    canvasWidth: Number,
    canvasHeight: Number,
    canvasMultiply: Number,
    projectName: String,
    videoPath:String,
    audio:String
}, { timestamps: true })

module.exports = mongoose.model("Video", Video)